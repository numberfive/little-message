package de.bit2atom.little_message;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;

public class MainActivity extends AppCompatActivity
        implements MessageSendDlg.NoticeDialogListener, AdapterView.OnItemClickListener, StateSendDlg.NoticeDialogListener
    {
        public static final Charset UTF_8 = Charset.forName("UTF-8");

    private static final int RESULT_SETTINGS = 1;
    private static final int RESULT_ALARM_DETAILS = 2;
    private DisplayMessageAdapter adapter = null;
    private ListView listView;
    private String deviceName;
    private boolean inShutdown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("MainActivity", "onCreate");
        setContentView(R.layout.activity_main);

        super.onCreate(savedInstanceState);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView title = findViewById(R.id.textViewTitle);
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        deviceName = sharedPrefs.getString("device_name", "NULL");
        title.append(deviceName);

        AppMainObject mainObject = AppMainObject.getInstance(this);
        mainObject.setListener(new AppMainObject.OnAppEventListener(){
            @Override
            public void onOpenWebSocket() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        TextView textView = findViewById(R.id.textViewError);
                        textView.setText("Connected");
                        FloatingActionButton fabMessage = findViewById(R.id.fabMessage);
                        fabMessage.setEnabled(true);
                        FloatingActionButton fabState = findViewById(R.id.fabState);
                        fabState.setEnabled(true);
                    }
                });
            }
            @Override
            public void onCloseWebSocket() {
                ShowCloseOnView("WebSocket Closed");
            }
            @Override
            public void onMessage(String message) {
                NewMessageIn(message);
            }
            @Override
            public void onErrorInfo(String message) {
                ShowErrorOnView(message);
            }
        });
        startupWebSocket();

        if(adapter == null) {
            adapter = new DisplayMessageAdapter(this, mainObject.getDisplayMessages());
        }

        View footerView = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.sample_list_footer_view, null, false);

        listView = findViewById(R.id.listViewMessages);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        listView.addFooterView(footerView);

        FloatingActionButton fabMessage = findViewById(R.id.fabMessage);
        fabMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HandleMessageButtonClick(view);
            }
        });
        fabMessage.setEnabled(false);

        FloatingActionButton fabState = findViewById(R.id.fabState);
        fabState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HandleStateButtonClick(view);
            }
        });
        fabState.setEnabled(false);
    }

    @Override
    public void onDestroy(){
        inShutdown = true;
        AppMainObject mainObject = AppMainObject.getInstance(this);
        mainObject.Destroy();

        super.onDestroy();
    }

    public void onItemClick(AdapterView parent, View view, int position, long id){
        //Snackbar.make(view, "testmessage", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        AppMainObject mainObject = AppMainObject.getInstance();
        if(mainObject.getDisplayMessages().get(position).isAlarmMessage()) {
            String text = mainObject.getDisplayMessages().get(position).getFullMessageText();
            String alarmId = mainObject.getDisplayMessages().get(position).getAlarmId();
            // Launching new Activity on selecting single List Item
            Intent i = new Intent(this, AlarmDetailsActivity.class);
            // sending data to new activity
            i.putExtra("text", text);
            i.putExtra("id", alarmId);
            startActivityForResult(i, RESULT_ALARM_DETAILS);
        }else {
            mainObject.getDisplayMessages().get(position).setShowFullText(!mainObject.getDisplayMessages().get(position).getShowFullText());
            adapter.notifyDataSetChanged();
        }

    }

    private void HandleMessageButtonClick(View view){
        AppMainObject mainObject = AppMainObject.getInstance();
        if(!mainObject.IsConnected()){
            Snackbar.make(view, "not connected please wait", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return;
        }
        DialogFragment dialog = new MessageSendDlg();
        dialog.show(getSupportFragmentManager(), getResources().getString(R.string.message_send_dlg_title));

    }

    private void HandleStateButtonClick(View view){
        AppMainObject mainObject = AppMainObject.getInstance();
        if(!mainObject.IsConnected()){
            Snackbar.make(view, "not connected please wait", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            return;
        }
        DialogFragment dialog = new StateSendDlg();
        dialog.show(getSupportFragmentManager(), getResources().getString(R.string.message_state_dlg_title));
    }

    @Override
    public void onMessageSendDlgPositiveClick(MessageSendDlg dialog) {
        // User touched the dialog's positive button
        Log.d("MainActivity", "MessageSendDlg Send Click");

        try {
            JSONObject jsonMessage = new JSONObject();
            jsonMessage.put("From", deviceName);
            jsonMessage.put("To", ""); //all
            jsonMessage.put("MessageType", "Text");
            jsonMessage.put("Text", dialog.MessageText);
            AppMainObject mainObject = AppMainObject.getInstance();
            mainObject.SendWebSocket(jsonMessage.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMessageSendDlgNegativeClick(MessageSendDlg dialog) {
        // User touched the dialog's negative button
        Log.d("MainActivity", "MessageSendDlg Cancel Click");
    }

    @Override
    public void onStateSendDlgPositiveClick(StateSendDlg dialog) {
        // User touched the dialog's positive button
        Log.d("MainActivity", "MessageSendDlg Send Click");

        try {
            JSONObject jsonMessage = new JSONObject();
            jsonMessage.put("From", deviceName);
            jsonMessage.put("To", ""); //all
            jsonMessage.put("MessageType", "State");
            jsonMessage.put("State", dialog.StateText);
            AppMainObject mainObject = AppMainObject.getInstance();
            mainObject.SendWebSocket(jsonMessage.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStateSendDlgNegativeClick(StateSendDlg dialog) {
        // User touched the dialog's negative button
        Log.d("MainActivity", "StateSendDlg Cancel Click");
    }
    private void startupWebSocket() {
        AppMainObject mainObject = AppMainObject.getInstance();
        try {
            mainObject.TryLogin();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void ShowErrorOnView(String message){
        final String textMessage = message;
        runOnUiThread(new Runnable() {
            public void run() {
                TextView textView = findViewById(R.id.textViewError);
                textView.setText(textMessage);
            }
        });
    }

    private void ShowCloseOnView(String message){
        if(inShutdown) return;

        final String textMessage = message;
        runOnUiThread(new Runnable() {
            public void run() {
                TextView textView = findViewById(R.id.textViewError);
                textView.setText(textMessage);
                reconnectWebSocket();
                FloatingActionButton fabMessage = findViewById(R.id.fabMessage);
                fabMessage.setEnabled(false);
                FloatingActionButton fabState = findViewById(R.id.fabState);
                fabState.setEnabled(false);
            }
        });
    }

    private void NewMessageIn(String message) {
        try {
            JSONObject jsonMessage = new JSONObject(message);
            String messageType = jsonMessage.getString("MessageType");
            Log.d("MainActivity", "NewMessageIn messageType " + messageType);
            if(messageType.contentEquals("Text"))
            {
                HandleNewTextMessage(message);
            }
            if(messageType.contentEquals("Alarm"))
            {
                HandleNewAlarmMessage(message);
            }
            if(messageType.contentEquals("Offline") || messageType.contentEquals("Online")) {
                HandleNewStateMessage(message);
            }
        }
        catch (JSONException e){
            Log.e("MainActivity", "NewMessageIn " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void addMessageGui(DisplayMessage theMessage){
        AppMainObject mainObject = AppMainObject.getInstance();
        mainObject.getDisplayMessages().add(theMessage);
        if(mainObject.getDisplayMessages().size() > 100){
            mainObject.getDisplayMessages().remove(0);
        }
        adapter.notifyDataSetChanged();
        listView.setSelection(mainObject.getDisplayMessages().size()-1);
    }

    private void addMessage(DisplayMessage theMessage){
        final DisplayMessage displayMessage = theMessage;
        runOnUiThread(new Runnable() {
            public void run() {
                addMessageGui(displayMessage);
            }
        });
    }
    private void HandleNewStateMessage(String textMessage) {
        try {
            JSONObject jsonObject = new JSONObject(textMessage);
            String messageType = jsonObject.getString("MessageType");
            String from = jsonObject.getString("From");
            String username = jsonObject.getString("Username");
            if(messageType.equals("Online")){
                addMessage(new DisplayMessage(deviceName, "User " + username + " Online", from, MessagesType.Status));
            }
            if(messageType.equals("Offline")){
                addMessage(new DisplayMessage(deviceName, "User " + username + " Offline", from, MessagesType.Status));
            }
        }
        catch (JSONException e){
            Log.e("MainActivity", "HandleNewMessage " + e.getMessage());
            e.printStackTrace();
        }
    }

        private void HandleNewAlarmMessage(String textMessage) {
            Log.d("MainActivity", "HandleNewAlarmMessage " + textMessage);
            try {
                JSONObject jsonObject = new JSONObject(textMessage);
                addMessage(new DisplayMessage(deviceName, jsonObject));
            }catch (JSONException e){
                Log.e("MainActivity", "HandleNewAlarmMessage " + e.getMessage());
                e.printStackTrace();
            }

            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            if(notification == null) {
                // alert backup is null, using 2nd backup
                notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
            //Todo Alarm never Stops
            if(notification != null){
                Ringtone ringTone = RingtoneManager.getRingtone(getApplicationContext(), notification);
                if(ringTone != null) {
                    ringTone.play();
                }
            }
        }

    private void HandleNewTextMessage(String textMessage) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        boolean playSound = false;
        Log.d("MainActivity", "HandleNewMessage " + textMessage);
        try {
            JSONObject jsonObject = new JSONObject(textMessage);
            String from = jsonObject.getString("From");
            if(!from.equals(deviceName)){
                playSound = true;
            }
            addMessage(new DisplayMessage(deviceName, jsonObject));
        }
        catch (JSONException e){
            Log.e("MainActivity", "HandleNewMessage " + e.getMessage());
            e.printStackTrace();
        }

        if(playSound){
            //todo sound form app !?
            if(!sharedPrefs.getBoolean("notifications_new_message", true)){
                return;
            }

            String sound = sharedPrefs.getString("notifications_new_message_ringtone","NULL");
            Log.d("MainActivity", "HandleNewMessage sound " + sound);
            Uri notification = Uri.parse(sound);
            if(notification == null) {
                notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                if(notification == null) {
                    // alert backup is null, using 2nd backup
                    notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                }

            }

            if(sharedPrefs.getBoolean("notifications_new_message_vibrate", true)){
                AppMainObject mainObject = AppMainObject.getInstance();
                mainObject.VibrateOneShot(150);
            }

            if(notification != null){
                Ringtone ringTone = RingtoneManager.getRingtone(getApplicationContext(), notification);
                if(ringTone != null) {
                    ringTone.play();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent i = new Intent(this, UserSettingsActivity.class);
            startActivityForResult(i, RESULT_SETTINGS);
            return true;
        }

        if (id == R.id.action_reconnect) {
            reconnectWebSocket();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean reconnectRunning;
    private void reconnectWebSocket(){
        if(reconnectRunning) return;

        reconnectRunning = true;
        AppMainObject mainObject = AppMainObject.getInstance();
        mainObject.CloseWebSocket();
        startupWebSocket();
        reconnectRunning = false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SETTINGS:
                settingChanged();
                break;

        }

    }

    private void settingChanged(){
        Log.d("MainActivity", "settingChanged");
        TextView tile = findViewById(R.id.textViewTitle);
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        deviceName = sharedPrefs.getString("device_name", "NULL");
        String text = getString(R.string.textViewTitle_Text);
        text += deviceName;
        tile.setText(text);
        AppMainObject mainObject = AppMainObject.getInstance();
        mainObject.ReloadConfig();

        reconnectWebSocket();
    }
}
