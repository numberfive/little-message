package de.bit2atom.little_message;

/**
 * Created by Michael Nenninger on 25.04.2016.
 */

public enum MessagesType{
    Status,
    Text,
    Alarm
}
