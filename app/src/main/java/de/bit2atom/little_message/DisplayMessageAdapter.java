package de.bit2atom.little_message;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Michael Nenninger on 25.04.2016.
 */



public class DisplayMessageAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<DisplayMessage> displayMessages;

    public DisplayMessageAdapter(Context context, ArrayList<DisplayMessage> messages) {
        super();
        this.mContext = context;
        this.displayMessages = messages;
    }

    @Override
    public int getCount() {
        return displayMessages.size();
    }

    @Override
    public Object getItem(int position) {
        return displayMessages.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DisplayMessage message = (DisplayMessage) this.getItem(position);
        ViewHolder holder;
        if(convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.message_row, parent, false);
            holder.message = convertView.findViewById(R.id.message_text);
            convertView.setTag(holder);
        }
        else
            holder = (ViewHolder) convertView.getTag();

        if( message.getShowFullText()){
            holder.message.setText(message.getFullMessageText());
        }else {
            holder.message.setText(message.getMessageText());
        }

        LayoutParams lp = (LayoutParams) holder.message.getLayoutParams();
        //check if it is a status message then remove background, and change text color.
        if(message.isStatusMessage())
        {
            int sdk = android.os.Build.VERSION.SDK_INT;
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                holder.message.setBackgroundDrawable(null);
            } else {
                holder.message.setBackground(null);
            }

            lp.gravity = Gravity.CENTER_HORIZONTAL;
            lp.setMargins(0,0,0,0);
            holder.message.setTextColor(ContextCompat.getColor(mContext,R.color.textFieldColor));
            holder.message.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);
            holder.message.setLayoutParams(lp);
        }
        else if(message.isAlarmMessage()){
            lp.setMargins(5,5,5,5);
            holder.message.setBackgroundResource(R.drawable.speech_bubble_alarm);
            lp.gravity = Gravity.CENTER_HORIZONTAL;
            holder.message.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            holder.message.setLayoutParams(lp);
            holder.message.setTextColor(ContextCompat.getColor(mContext,R.color.textColor));
        }
        else
        {
            lp.setMargins(5,5,5,5);
            //Check whether message is mine to show green background and align to right
            if(message.isMe())
            {
                holder.message.setBackgroundResource(R.drawable.speech_bubble_green);
                lp.gravity = Gravity.END;
            }
            //If not mine then it is from sender to show orange background and align to left
            else
            {
                holder.message.setBackgroundResource(R.drawable.speech_bubble_orange);
                lp.gravity = Gravity.START;
            }
            holder.message.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            holder.message.setLayoutParams(lp);
            holder.message.setTextColor(ContextCompat.getColor(mContext,R.color.textColor));
        }

        return convertView;
    }

    @Override
    public long getItemId(int position) {
        //Unimplemented, because we aren't using Sqlite.
        return position;
    }

    private static class ViewHolder
    {
        TextView message;
    }
}
