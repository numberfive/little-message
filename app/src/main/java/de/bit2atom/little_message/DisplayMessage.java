package de.bit2atom.little_message;

import org.json.JSONException;
import org.json.JSONObject;

public class DisplayMessage {
    private String OwnName;
    /**
     * The content of the message
     */
    private String MessageText;

    /**
     * How has send the MessageText empty => me
     */
    private String From;

    private MessagesType Type;
    private boolean showFullText;
    private String AlarmId;

    public DisplayMessage(String ownName, String messageText, String from, MessagesType type){
        super();
        MessageText = messageText;
        From = from;
        OwnName = ownName;
        Type = type;
        showFullText = false;
    }

    public DisplayMessage(String ownName, JSONObject jsonMessage) throws JSONException{
        super();
        OwnName = ownName;
        MessageText = jsonMessage.getString("Text");
        From = jsonMessage.getString("From");
        String messageType = jsonMessage.getString("MessageType");
        if(messageType.contentEquals("Alarm")){
            Type = MessagesType.Alarm;
            AlarmId = jsonMessage.getString("AlarmId");
        } else {
            Type = MessagesType.Text;
        }
        showFullText = false;
    }

    public String getMessageText() {
        String message =  MessageText;
        message = message.replace("\n","");
        if(message.length() > 27){
            message = message.substring(0,24) + "...";
        }
        return message;
    }

    public String getFullMessageText(){return MessageText;}

    public boolean isStatusMessage(){
        return Type == MessagesType.Status;
    }

    public boolean isAlarmMessage(){
        return Type == MessagesType.Alarm;
    }

    public boolean getShowFullText(){return showFullText;}

    public String getAlarmId(){return AlarmId;}

    public void setShowFullText(boolean value){showFullText = value;}

    public boolean isMe() {

        if(From == "")
            return true;
        if(From.equals(OwnName))
            return true;

        return false;
    }
}
