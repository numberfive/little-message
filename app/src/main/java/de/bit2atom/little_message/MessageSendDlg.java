package de.bit2atom.little_message;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by michael.nenninger on 28.04.2016.
 */

public class MessageSendDlg extends DialogFragment {
    public String MessageText;
    private TextView sendToText;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_message_send, null);
        sendToText = (TextView)view.findViewById(R.id.textViewSendTo);
        sendToText.setText(R.string.message_send_dlg_all);

        Button buttonOne = (Button) view.findViewById(R.id.buttonChoose);
        buttonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "not implemented", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        });

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view)
                // Add action buttons
                .setPositiveButton(R.string.message_send_dlg_send, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Send the positive button event back to the host activity
                        EditText messageEdit = (EditText)MessageSendDlg.this.getDialog().findViewById(R.id.editTextMessage);
                        MessageText = messageEdit.getText().toString();
                        mListener.onMessageSendDlgPositiveClick(MessageSendDlg.this);
                    }
                })
                .setNegativeButton(R.string.message_send_dlg_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Send the negative button event back to the host
                        mListener.onMessageSendDlgNegativeClick(MessageSendDlg.this);
                    }


                });
        return builder.create();
    }

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface NoticeDialogListener {
        void onMessageSendDlgPositiveClick(MessageSendDlg dialog);
        void onMessageSendDlgNegativeClick(MessageSendDlg dialog);
    }

    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

}
