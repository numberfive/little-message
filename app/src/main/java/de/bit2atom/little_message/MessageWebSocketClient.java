package de.bit2atom.little_message;

import android.util.Log;

import org.java_websocket.client.DefaultSSLWebSocketClientFactory;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft_17;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by michael.nenninger on 14.05.2016.
 * SSL http://stackoverflow.com/questions/34627959/draft-refuses-handshake-when-using-java-websocket-library-to-connect-to-the-coin
 * https://github.com/TooTallNate/Java-WebSocket
 */
public class MessageWebSocketClient extends WebSocketClient {

    public MessageWebSocketClient(URI uri, Map<String,String> headers) {
        super(uri,new Draft_17(),headers,1000);
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return new java.security.cert.X509Certificate[]{};
            }


            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }


            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {
            }
        }};
        if(uri.getScheme().equals("wss")){
            try {
                SSLContext sc = SSLContext.getInstance("TLS");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                super.setWebSocketFactory(new DefaultSSLWebSocketClientFactory(sc));
            }
            catch (Exception e) {
                e.printStackTrace();
                Log.i("SSL", "Error " + e.getMessage());
                return;
            }
        }
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {

    }

    @Override
    public void onMessage(String message) {

    }

    @Override
    public void onClose(int code, String reason, boolean remote) {

    }

    @Override
    public void onError( Exception ex ) {
        ex.printStackTrace();

    }
}
