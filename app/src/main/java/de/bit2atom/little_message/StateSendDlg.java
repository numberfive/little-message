package de.bit2atom.little_message;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

/**
 * Created by michael.nenninger on 20.05.2016.
 */
public class StateSendDlg extends DialogFragment {
    public String StateText;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.dialog_state_send, null);
        Button buttonOne = (Button) view.findViewById(R.id.buttonOne);
        buttonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StateText = "1";
                mListener.onStateSendDlgPositiveClick(StateSendDlg.this);
                dismiss();
            }
        });

        Button buttonTwo = (Button) view.findViewById(R.id.buttonTwo);
        buttonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StateText = "2";
                mListener.onStateSendDlgPositiveClick(StateSendDlg.this);
                dismiss();
            }
        });

        Button buttonThree = (Button) view.findViewById(R.id.buttonThree);
        buttonThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StateText = "3";
                mListener.onStateSendDlgPositiveClick(StateSendDlg.this);
                dismiss();
            }
        });

        Button buttonFour = (Button) view.findViewById(R.id.buttonFour);
        buttonFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StateText = "4";
                mListener.onStateSendDlgPositiveClick(StateSendDlg.this);
                dismiss();
            }
        });

        Button buttonFive = (Button) view.findViewById(R.id.buttonFive);
        buttonFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StateText = "5";
                mListener.onStateSendDlgPositiveClick(StateSendDlg.this);
                dismiss();
            }
        });

        Button buttonSix = (Button) view.findViewById(R.id.buttonSix);
        buttonFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StateText = "6";
                mListener.onStateSendDlgPositiveClick(StateSendDlg.this);
                dismiss();
            }
        });

        builder.setView(view)
                // Add action buttons
                .setNegativeButton(R.string.message_send_dlg_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Send the negative button event back to the host
                        mListener.onStateSendDlgNegativeClick(StateSendDlg.this);
                    }


                });
        return builder.create();
    }

    /* The activity that creates an instance of this dialog fragment must
     * implement this interface in order to receive event callbacks.
     * Each method passes the DialogFragment in case the host needs to query it. */
    public interface NoticeDialogListener {
        void onStateSendDlgPositiveClick(StateSendDlg dialog);
        void onStateSendDlgNegativeClick(StateSendDlg dialog);
    }

    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }
}
