package de.bit2atom.little_message;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.VIBRATOR_SERVICE;

/**
 * Created by michael.nenninger on 15.05.2016.
 */
class AppMainObject {

    private static AppMainObject instance;
    private ArrayList<DisplayMessage> displayMessages;
    private Context _context;
    private String server;
    private String loginName;
    private String loginPassword;
    private String token;
    private MessageWebSocketClient webSocketClient;

    private OnAppEventListener eventListener;

    private AppMainObject(Context context){
        _context = context;
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(_context);

        String deviceName = sharedPrefs.getString("device_name", "NULL");
        server = sharedPrefs.getString("server_address", "NULL");
        loginName = sharedPrefs.getString("login_name", "test");
        loginPassword = sharedPrefs.getString("login_password", "1234");
        displayMessages = new ArrayList<>();
        displayMessages.add(new DisplayMessage(deviceName, "Connecting", "",MessagesType.Status));

        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(loginName, loginPassword.toCharArray());
            }
        });

      }

    static AppMainObject getInstance(Context context) {
        if(instance == null){
            instance = new AppMainObject(context);
        }
        return instance;
    }

    static AppMainObject getInstance() {
        return instance;
    }

    ArrayList<DisplayMessage> getDisplayMessages() {
        return displayMessages;
    }

    void AcceptAlarm(final String alarmId){
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try{
                    System.setProperty("http.keepAlive", "false");
                    URL url = new URL("http://"+server+":8020/api/alarm/"+alarmId+"/accept");
                    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setDoOutput(true);
                    urlConnection.setUseCaches(false);
                    urlConnection.setReadTimeout(10000 /* milliseconds */);
                    urlConnection.setConnectTimeout(15000 /* milliseconds */);
                    urlConnection.connect();
                    try {
                        /*PrintWriter out = new PrintWriter(urlConnection.getOutputStream());
                        //out.print(param);
                        out.close();*/

                        if(urlConnection.getResponseCode() == HttpURLConnection.HTTP_ACCEPTED) {
                            //InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                            //readStream(in);
                        }
                        urlConnection.disconnect();
                    }
                    catch(Exception exp){
                        String message = exp.getMessage();
                        if(message == null){
                            message = "no message";
                        }
                        Log.e("SendAcceptRequest",message);
                        exp.printStackTrace();
                    }
                }
                catch (MalformedURLException exp){
                    Log.e("SendAcceptRequest",exp.getMessage());
                    exp.printStackTrace();
                } catch (IOException exp) {
                    Log.e("SendAcceptRequest",exp.getMessage());
                    exp.printStackTrace();
                }
            }
        });
        thread.start();
    }

    private boolean GetToken() {
        try {
            System.setProperty("http.keepAlive", "false");
            URL url = new URL("http://" + server + "/api/users/authenticate");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setUseCaches(false);
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestProperty("Content-Type","application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.connect();
            try {
                JSONObject auth=new JSONObject();
                auth.put("Username", loginName);
                auth.put("Password", loginPassword);
                OutputStream os = urlConnection.getOutputStream();
                os.write(auth.toString().getBytes("UTF-8"));
                os.close();

                if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    String result = readResponseString(in);
                    JSONObject jsonObject = new JSONObject(result);
                    token = jsonObject.get("token").toString();
                    in.close();
                }

                urlConnection.disconnect();
            } catch (Exception exp) {
                String message = exp.getMessage();
                if (message == null) {
                    message = "no message";
                }
                Log.e("SendAcceptRequest", message);
                exp.printStackTrace();
                return false;
            }
        }
        catch (MalformedURLException exp){
            Log.e("SendAcceptRequest",exp.getMessage());
            exp.printStackTrace();
            if (eventListener != null) {
                eventListener.onErrorInfo(exp.getMessage());
            }
            return false;
        } catch (IOException exp) {
            Log.e("SendAcceptRequest",exp.getMessage());
            exp.printStackTrace();
            if (eventListener != null) {
                eventListener.onErrorInfo(exp.getMessage());
            }
            return false;
        }

        return true;
    }

    private String readResponseString(InputStream entityResponse) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;
        while ((length = entityResponse.read(buffer)) != -1) {
            baos.write(buffer, 0, length);
        }
        return baos.toString();
    }

    private void StartWebSocket(){
        try {
            Map<String, String> headers = new HashMap<>();
            headers.put("Sec-WebSocket-Protocol","client, " + token);
            URI url = new URI("http://" + server + "/api/message");
            webSocketClient = new MessageWebSocketClient(url, headers) {
                @Override
                public void onOpen(ServerHandshake serverHandshake) {
                    Log.i("Websocket", "Opened");
                    if (eventListener != null) {
                        eventListener.onOpenWebSocket();
                    }
                }
                @Override
                public void onClose(int i, String s, boolean remote) {
                    Log.i("Websocket", "Closed " + s + " remote " + remote);
                    if (eventListener != null) {
                        eventListener.onCloseWebSocket();
                    }
                    webSocketClient = null;
                }
                @Override
                public void onMessage(String message) {
                    if (eventListener != null) {
                        eventListener.onMessage(message);
                    }
                }
                @Override
                public void onError(Exception e) {
                    String message = e.getMessage();
                    if(message == null){
                        message = e.toString();
                    }
                    Log.i("Websocket", "Error " + message);
                    //ShowErrorOnView("Websocket Error " + message);
                    webSocketClient = null;
                }
            };
            webSocketClient.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
            token = null;
        }
    }

    void TryLogin() throws InterruptedException {
        if(token == null) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    if (GetToken()) {
                        StartWebSocket();
                    }

                }
            });
            thread.start();
        } else {
            if(webSocketClient != null){
                WebSocket.READYSTATE currentState = webSocketClient.getReadyState();
                if(currentState.equals( WebSocket.READYSTATE.CONNECTING) ||
                   currentState.equals( WebSocket.READYSTATE.NOT_YET_CONNECTED)){
                    Log.d("Websocket", "please wait connection is in work");
                    return;
                }

                if(currentState.equals( WebSocket.READYSTATE.OPEN)){
                    Log.d("Websocket", "already connected");
                    return;
                }

                if(currentState.equals( WebSocket.READYSTATE.CLOSING)){
                    Log.d("Websocket", "in closing");
                    do{
                        Thread.sleep(10);
                        if(webSocketClient == null) break;
                        currentState = webSocketClient.getReadyState();
                    } while(currentState == WebSocket.READYSTATE.CLOSING);

                }
            }

            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    StartWebSocket();
                }
            });
            thread.start();
        }
    }

    void ReloadConfig() {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(_context);

        String deviceName = sharedPrefs.getString("device_name", "NULL");
        server = sharedPrefs.getString("server_address", "NULL");
        loginName = sharedPrefs.getString("login_name", "test");
        loginPassword = sharedPrefs.getString("login_password", "1234");

        displayMessages.add(new DisplayMessage(deviceName, "Willkommen", "",MessagesType.Text));
    }

    void Destroy() {
        CloseWebSocket();
    }

    boolean IsConnected() {
        return webSocketClient != null && webSocketClient.getReadyState() == WebSocket.READYSTATE.OPEN;
    }

    void SendWebSocket(String message) {
        if(webSocketClient == null || webSocketClient.getReadyState() != WebSocket.READYSTATE.OPEN){
            Log.e("AppMainObject", "WebSocket not connected");
            return;
        }
        webSocketClient.send(message);
    }

    void CloseWebSocket() {
        if(webSocketClient != null && webSocketClient.getReadyState() == WebSocket.READYSTATE.OPEN){
            try {
                webSocketClient.closeBlocking();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    void setListener(OnAppEventListener listener) {
        eventListener = listener;
    }

    public void VibrateOneShot(int msec){
        Vibrator vibrator = (Vibrator)_context.getSystemService(VIBRATOR_SERVICE);
        if(vibrator != null) {
            if (Build.VERSION.SDK_INT >= 26) {
                vibrator.vibrate(VibrationEffect.createOneShot(msec, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                vibrator.vibrate(msec);
            }
        }
    }

    public interface OnAppEventListener {
        void onOpenWebSocket();
        void onCloseWebSocket();
        void onMessage(String message);
        void onErrorInfo(String message);
    }
}
